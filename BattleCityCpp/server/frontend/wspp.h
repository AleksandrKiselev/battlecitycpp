/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <mutex>
#include <string>
#include <unordered_map>
#include "server/websocketpp/common/connection_hdl.hpp"
#include "server/websocketpp/config/asio_no_tls.hpp"
#include "server/websocketpp/server.hpp"

namespace battle_city_cpp {

using ConnectionHdl = websocketpp::connection_hdl;
using WebSocket = websocketpp::server<websocketpp::config::asio>;
using ConnectionPtr = WebSocket::connection_ptr;
using MessagePtr = WebSocket::message_ptr;

using ConnectionId = std::string;

class Connections {
public:
  Connections() = default;
  ~Connections() = default;

  ConnectionPtr& operator[](ConnectionId id) {
    std::lock_guard<std::mutex> lock{ mutex_ };
    return connections_[id];
  }

  void remove(ConnectionId id) {
    std::lock_guard<std::mutex> lock{ mutex_ };
    connections_.erase(id);
  }

  void clear() {
    std::lock_guard<std::mutex> lock{ mutex_ };
    connections_.clear();
  }

private:
  std::mutex mutex_;
  std::unordered_map<ConnectionId, ConnectionPtr> connections_;
};

} // namespace battle_city_cpp
