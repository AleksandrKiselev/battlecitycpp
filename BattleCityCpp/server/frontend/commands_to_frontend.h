/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <functional>
#include "server/service/command_to.h"
#include "server/frontend/frontend_service.h"


namespace battle_city_cpp {
namespace commands_to_frontend {

class AddSessionCommand : public CommandTo<FrontendService> {
public:

  AddSessionCommand(const ConnectionId& conn_id, UserProfileSPtr user)
    : conn_id_{ conn_id }
    , user_{ user } {
  }
  virtual ~AddSessionCommand() = default;

  virtual bool execute(FrontendService* service) final {
    return service->addSession(conn_id_, user_);
  }

private:
  ConnectionId conn_id_;
  UserProfileSPtr user_;
};


class RemoveSessionCommand : public CommandTo<FrontendService> {
public:
  RemoveSessionCommand(const ConnectionId& conn_id) : conn_id_{ conn_id } {}
  virtual ~RemoveSessionCommand() = default;

  virtual bool execute(FrontendService* service) final {
    return service->removeSession(conn_id_);
  }

private:
  ConnectionId conn_id_;
};


class SendByIdCommand : public CommandTo<FrontendService> {
public:
  SendByIdCommand(const ConnectionId& conn_id, const std::string& msg)
    : conn_id_{ conn_id }
    , msg_{ msg } {
  }

  virtual ~SendByIdCommand() = default;

  virtual bool execute(FrontendService* service) final {
    return service->send(conn_id_, msg_);
  }

private:
  ConnectionId conn_id_;
  std::string msg_;
};


class SendByUserCommand : public CommandTo<FrontendService> {
public:
  SendByUserCommand(UserProfileSPtr user, const std::string& msg)
    : user_{ user }
    , msg_{ msg } {
  }

  virtual ~SendByUserCommand() = default;

  virtual bool execute(FrontendService* service) final {
    return service->send(user_, msg_);
  }

private:
  UserProfileSPtr user_;
  std::string msg_;
};

} // namespace commands_to_frontend
} // namespace battle_city_cpp
