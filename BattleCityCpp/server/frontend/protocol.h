/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <string>
#include <unordered_map>

namespace battle_city_cpp {
namespace protocol {

const std::string cOpType{ "oper" };
const std::string cTime{ "time" };
const std::string cName{ "name" };
const std::string cId{ "id" };
const std::string cXCoord{ "x" };
const std::string cYCoord{ "y" };
const std::string cState{ "state" };

enum class OpType {
  NONE,
  LOGIN,
  LOGOUT,
  TOP,
  BOTTOM,
  LEFT,
  RIGHT,
  STOP,
  FIRE,
  WORLD,
  SYNC,
  INFO
};

const std::unordered_map<OpType, std::string> opTypeNames {
  { OpType::LOGIN,  "login"  },
  { OpType::LOGOUT, "logout" },
  { OpType::TOP,    "top"    },
  { OpType::BOTTOM, "bottom" },
  { OpType::LEFT,   "left"   },
  { OpType::RIGHT,  "right"  },
  { OpType::STOP,   "stop"   },
  { OpType::FIRE,   "fire"   },
  { OpType::WORLD,  "world"  },
  { OpType::SYNC,   "sync"   },
  { OpType::INFO,   "info"   }
};

extern inline 
OpType getOpTypeValue(const std::string& str) {
  for (auto& s : opTypeNames) {
    if (s.second == str)
      return s.first;
  }
  return OpType::NONE;
}

extern inline
std::string getOpTypeName(OpType type) {
  if (!opTypeNames.count(type))
    return std::string{};
  return opTypeNames.at(type);
}


namespace request {
/*==========================================================================
                        From client
============================================================================
Format: { "time":<timestamp>, "oper":<OpType>, ... }
----------------------------------------------------------------------------
{ "time":<timestamp>, "oper":"login", "name":<string> }
{ "time":<timestamp>, "oper":"logout" }
{ "time":<timestamp>, "oper":"top" }
{ "time":<timestamp>, "oper":"bottom" }
{ "time":<timestamp>, "oper":"left" }
{ "time":<timestamp>, "oper":"right" }
{ "time":<timestamp>, "oper":"world" }
{ "time":<timestamp>, "oper":"sync", "x":<int>, "y":<int>, "state":<state> }
{ "time":<timestamp>, "oper":"info", }
==========================================================================*/
} // namespace request


enum class OpResult {
  SUCCESS,
  FAIL
};

const std::unordered_map<OpResult, std::string> opResultNames {
  { OpResult::SUCCESS, "success" },
  { OpResult::FAIL,    "fail" },
};


namespace response { 
/*==========================================================================
                         To client
============================================================================
Format: { "time":<timestamp>, "oper":<OpType>, "result":<OpResult>, ... }
----------------------------------------------------------------------------

==========================================================================*/
} // namespace response


} // namespace protocol
} // namespace battle_city_cpp
