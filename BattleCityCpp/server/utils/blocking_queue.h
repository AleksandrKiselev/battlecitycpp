/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <deque>
#include <mutex>
#include <memory>
#include <utility>
#include <type_traits>
#include <condition_variable>


namespace battle_city_cpp {

template<typename T>
class BlockingQueue {
public:

  template<
    typename V,
    typename = std::enable_if_t< std::is_same<std::decay_t<V>, T>::value >
  >
  void push(V&& value) {
    std::lock_guard<std::mutex> lock{ mutex_ };
    queue_.push_back(std::forward<V>(value));
    cvar_.notify_one();
  }


  T pop() {
    std::unique_lock<std::mutex> cvlock{ cvmutex_ };
    cvar_.wait(cvlock, [&]() { return !empty(); });

    std::lock_guard<std::mutex> lock{ mutex_ };
    T front{ std::move(queue_.front()) };
    queue_.pop_front();
    return front;
  }


  std::size_t size() const {
    std::lock_guard<std::mutex> lock{ mutex_ };
    return queue_.size();
  }


  bool empty() const {
    std::lock_guard<std::mutex> lock{ mutex_ };
    return queue_.empty();
  }


private:
  std::deque<T> queue_;
  mutable std::mutex mutex_;
  std::condition_variable cvar_;
  std::mutex cvmutex_;
};

} // namespace battle_city_cpp
