/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <memory>
#include "server/frontend/frontend_service.h"
#include "server/account/commands_to_account.h"


namespace battle_city_cpp {

FrontendService::FrontendService(CommandQueueSPtr queue, ParserInterfaceSPtr parser)
  : Service{ queue }
  , parser_{ parser } {
}


bool FrontendService::addSession(const ConnectionId& conn_id, UserProfileSPtr user) {
  if (!user) 
    return false;

  ConnectionPtr conn = connections_[conn_id];
  return sessions_.add(conn, user);
}


bool FrontendService::removeSession(const ConnectionId& conn_id) {
  ConnectionPtr conn = connections_[conn_id];
  return sessions_.remove(conn);
}


void FrontendService::disconnect(ConnectionPtr conn) {
  assert(conn);

  ConnectionId id{ connectionId(conn) };
  connections_.remove(id);
  socket_.close(conn->get_handle(), 401, "disconnected");

  if (UserProfileSPtr user = sessions_.findUser(conn)) {
    removeSession(id);
    send_command( 
      std::make_unique<commands_to_account::RemoveUserCommand>(id, user->name(), false) 
    );
  }
}


bool FrontendService::send(const ConnectionId& conn_id, const std::string& msg) {
  if (msg.empty()) 
    return false;

  ConnectionPtr conn = connections_[conn_id];
  if (!conn) 
    return false;

  socket_.send(conn->get_handle(), msg, websocketpp::frame::opcode::text);
  return true;
}


bool FrontendService::send(UserProfileSPtr user, const std::string& msg) {
  if (!user || msg.empty()) 
    return false;

  ConnectionPtr conn = sessions_.findConnection(user);
  if (!conn) 
    return false;

  socket_.send(conn->get_handle(), msg, websocketpp::frame::opcode::text);
  return true;
}


bool FrontendService::broadcast(const std::string& msg) {
  if (msg.empty()) 
    return true;

  sessions_.forEach(
    [&](const ConnectionHdl& hdl) {
      socket_.send(hdl, msg, websocketpp::frame::opcode::text);
    }
  );
  return true;
}


bool FrontendService::run(int port) {
  sessions_.clear();
  connections_.clear();
  socket_.init_asio();


  socket_.set_open_handler([&](ConnectionHdl hdl) {
    try {
      ConnectionPtr conn{ socket_.get_con_from_hdl(hdl) };
      ConnectionId id{ connectionId(conn) };
      connections_[id] = conn;
    }
    catch (const std::exception& e) {
    }
  });


  socket_.set_close_handler([&](ConnectionHdl hdl) {
    try {
      ConnectionPtr conn{ socket_.get_con_from_hdl(hdl) };
      disconnect(conn);
    }
    catch (const std::exception& e) {
    }
  });


  socket_.set_message_handler([&](ConnectionHdl hdl, MessagePtr msg) {
    try {
      ConnectionPtr conn{ socket_.get_con_from_hdl(hdl) };
      ConnectionId id{ connectionId(conn) };
      std::string req{ msg->get_payload() };

      assert(parser_);
      if (CommandInterfaceUPtr command = parser_->parse(id, req)) {
        send_command(std::move(command));
      }
    }
    catch (const std::exception& e) {
    }
  });


  try{
    socket_.listen(port);
    socket_.start_accept();
    socket_.run(); 
  }

  catch (const std::exception& e) {
    return false;
  }
  return true;
}


ConnectionId FrontendService::connectionId(ConnectionPtr conn) {
  assert(conn);
  return ConnectionId{ conn->get_remote_endpoint() };
}


} // namespace battle_city_cpp
