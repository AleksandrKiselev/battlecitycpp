/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#include "server/frontend/sessions.h"


namespace battle_city_cpp {

bool Sessions::add(ConnectionPtr conn, UserProfileSPtr user) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };

  if (sessions_map_[conn])
    return false;

  sessions_map_[conn] = user;
  return true;
}


UserProfileSPtr Sessions::findUser(const ConnectionHdl& hdl) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  return findUser(findConnection(hdl));
}


UserProfileSPtr Sessions::findUser(ConnectionPtr conn) {
  if (!conn)
    return nullptr;

  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  return sessions_map_[conn];
}


ConnectionPtr Sessions::findConnection(const ConnectionHdl& hdl) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  if (auto& conn_ptr = hdl.lock()) {
    for (auto& s : sessions_map_) {
      if (s.first == conn_ptr) {
        return s.first;
      }
    }
  }
  return nullptr;
}


ConnectionPtr Sessions::findConnection(UserProfileSPtr user) {
  if (!user)
    return nullptr;

  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  for (auto& s : sessions_map_) {
    if (s.second == user) {
      return s.first;
    }
  }

  return nullptr;
}


bool Sessions::remove(ConnectionPtr conn) {
  if (!conn)
    return false;

  std::lock_guard<std::recursive_mutex> lock{ mutex_ };

  return sessions_map_.erase(conn) > 0;
}


bool Sessions::remove(UserProfileSPtr user) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  return remove(findConnection(user));
}


bool Sessions::remove(const ConnectionHdl& hdl) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  return remove(findConnection(hdl));
}


void Sessions::clear() {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  sessions_map_.clear();
}


void Sessions::forEach(SessionFunction func) {
  std::lock_guard<std::recursive_mutex> lock{ mutex_ };
  for (auto& s : sessions_map_) {
    func(s.first);
  }
}


} // namespace battle_city_cpp
