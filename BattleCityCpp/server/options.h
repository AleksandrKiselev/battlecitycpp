/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <string>
#include <iostream>
#include <boost/program_options.hpp>


namespace battle_city_cpp {

class Options {
public:
  static Options& inst() {
    static Options options;
    return options;
  }


  bool load(int ac, char** av) {
    namespace po = boost::program_options;

    po::options_description desc("Required options");
    desc.add_options()
      ("port,P", po::value<unsigned>()->required(), "Set port number")
      ;

    po::variables_map vm;
    try {
      po::store(po::parse_command_line(ac, av, desc), vm);
      po::notify(vm);
    } 
    catch (po::error& e) {
      std::cerr << e.what() << std::endl;
      return false;
    }

    port_ = vm["port"].as<unsigned>();
    return true;
  }

  int port() const { return port_; }
  std::string usage() const { return cUsage_; }


private:
  const std::string cUsage_{ "server.exe -P<port_number>" };

  Options() = default;

  int port_{ 0 };
};

} // namespace battle_city_cpp