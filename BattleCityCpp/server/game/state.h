/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <deque>
#include <type_traits>
#include <unordered_map>
#include "server/utils/sp_generator.h"
#include "server/utils/id_generator.h"
#include "server/utils/remove_ref_ptr.h"


namespace battle_city_cpp {

class State;
class StateObject;
GENERATE_SMART_POINTERS(State);

class State {
public:
  virtual ~State() = default;
  virtual void enter(StateObject* object) const = 0;
  virtual void handle(StateObject* object) const = 0;
  virtual void exit(StateObject* object) const = 0;


  // Flyweight pattern realization for State objects
  template<
    typename S,
    typename = std::enable_if_t< std::is_base_of<State, S>::value >
  >
  static const State* get() {
    Id id = IdGenerator::typeId<remove_ref_ptr<S>>();
    if (!states_map_.count(id))
      states_map_[id] = std::move(std::make_unique<S>());

    return states_map_[id].get();
  }

private:
  static std::unordered_map<Id, StateUPtr> states_map_;
};


class EmptyState final : public State {
public:
  virtual void enter(StateObject* object) const final {}
  virtual void handle(StateObject* object) const final {}
  virtual void exit(StateObject* object) const final {}
};



class StateObject {
public:
  StateObject() = default;
  virtual ~StateObject() = default;

  const State* popState() {
    if (!states_.empty()) {
      if (const State* state = states_.front()) {
        states_.pop_front();
        state->exit(this);
        return state;
      }
    }
    return State::get<EmptyState>();
  }

  void pushState(const State* state) {
    if (nullptr != state) {
      state->enter(this);
      states_.push_back(state);
    }
  }

  void setState(const State* state) {
    states_.clear();
    pushState(state);
  }

  void handleState() {
    popState()->handle(this);
  }

private:
  std::deque<const State*> states_;
};


} // namespace battle_city_cpp

