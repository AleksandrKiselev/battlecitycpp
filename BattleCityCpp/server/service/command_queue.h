/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <cassert>
#include <memory>
#include <unordered_map>
#include "server/service/command_interface.h"
#include "server/utils/blocking_queue.h"
#include "server/utils/sp_generator.h"


namespace battle_city_cpp {

using CommandBlockingQueue = BlockingQueue<CommandInterfaceUPtr>;

GENERATE_SMART_POINTERS(CommandBlockingQueue);

using CommandMap = std::unordered_map<Address, CommandBlockingQueueUPtr>;


class CommandQueue {
public:
  CommandQueue() = default;
  ~CommandQueue() = default;

  virtual void send(CommandInterfaceUPtr command) final {
    assert(command);
    Address to = command->to();
    queue(to)->push(std::move(command));
  }

  virtual CommandInterfaceUPtr recv(Address to) final {
    return queue(to)->pop();
  }

private:
  CommandMap c_map_;

  CommandBlockingQueue* queue(Address to) {
    if (!c_map_.count(to))
      c_map_[to] = std::make_unique<CommandBlockingQueue>();
    return c_map_[to].get();
  }
};

GENERATE_SMART_POINTERS(CommandQueue);

} // namespace battle_city_cpp
