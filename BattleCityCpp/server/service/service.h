/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#pragma once
#include <string>
#include <cassert>
#include "server/service/address_generator.h"
#include "server/service/service_interface.h"
#include "server/service/command_queue.h"
#include "server/utils/name_generator.h"
#include "server/utils/sp_generator.h"


namespace battle_city_cpp {

template<typename DerivedT>
class Service : public ServiceInterface {
public:
  Service(CommandQueueSPtr queue)
    : address_{ AddressGenerator::typeAddress<DerivedT>() }
    , name_{ NameGenerator::typeName<DerivedT>() }
    , queue_{ queue } {
  }
  virtual ~Service() = default;

  virtual Address address() const final { return address_; }
  virtual std::string name() const final { return name_; }

  void send_command(CommandInterfaceUPtr command) {
    assert(queue_);
    if (!command) return;
    queue_->send(std::move(command));
  }

  CommandInterfaceUPtr recv_command() {
    assert(queue_);
    return queue_->recv(address());
  }


private:
  Address address_;
  std::string name_;
  CommandQueueSPtr queue_;
};

} // namespace battle_city_cpp
