/*

  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/


#include <memory>
#include <iostream>

#include "server/options.h"

#include "server/service/worker.h"
#include "server/service/worker_pool.h"
#include "server/service/command_executor.h"
#include "server/service/function_executor.h"

#include "server/account/account_service.h"
#include "server/frontend/frontend_service.h"
#include "server/frontend/request/parser_json.h"




int main(int argc, char** argv) {
  using namespace battle_city_cpp;

  Options& options{ Options::inst() };
  if (!options.load(argc, argv)) {
    std::cout << options.usage();
    return 0;
  }

  CommandQueueSPtr queue{ std::make_shared<CommandQueue>() };
  AccountServiceSPtr account{ std::make_shared<AccountService>(queue) };

  ParserInterfaceSPtr parser{ std::make_shared<ParserJSON>() };
  FrontendServiceSPtr frontend{ std::make_shared<FrontendService>(queue, parser) };

  WorkerPool pool;
  pool.add(std::make_unique<CommandExecutor>(account, queue));
  pool.add(std::make_unique<CommandExecutor>(frontend, queue));

  auto frontend_run_function = [&]() {
    if (!frontend->run(options.port())) {
      pool.stop(false);
    }
  };

  pool.add(std::make_unique<FunctionExecutor>(frontend_run_function));
  pool.start();

  return 0;
}