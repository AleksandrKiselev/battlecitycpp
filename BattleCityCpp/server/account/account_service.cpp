/*
 	
  Copyright (c) 2016, Aleksandr Kiselev. All rights reserved.
 
  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the BattleCityCpp solution nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.
 
  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
  ARE DISCLAIMED. IN NO EVENT SHALL PETER THORSON BE LIABLE FOR ANY
  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
*/


#include "server/account/account_service.h"
#include <algorithm>


namespace battle_city_cpp {

AccountService::AccountService(CommandQueueSPtr queue)
  : Service{ queue } 
  , name_regex_{ "^[A-Za-z0-9_]{3,30}$" } {
}


bool AccountService::valid(const std::string& name) const {
  return std::regex_match(name, name_regex_);
}


bool AccountService::exist(const std::string& name) const {
  return find(name) != nullptr;
}


UserProfileSPtr AccountService::find(const std::string& name) const {
  auto it = std::find_if(
    users_.begin(),
    users_.end(),
    [&name](UserProfileSPtr u) {
      return u->name() == name;
    }
  );

  UserProfileSPtr user;
  if (it != users_.end()) {
    user = *it;
  }

  return user;
}


UserProfileSPtr AccountService::add(const std::string& name) {
  std::lock_guard<std::mutex> lock{ mutex_ };
  if (!valid(name) || exist(name))
    return nullptr;

  UserProfileSPtr user{ std::make_shared<UserProfile>(name) };
  users_.push_back(user);
  return user;
}


bool AccountService::remove(const std::string& name) {
  std::lock_guard<std::mutex> lock{ mutex_ };

  bool removed{ false };

  std::remove_if(
    users_.begin(),
    users_.end(),
    [&removed, &name](UserProfileSPtr u) {
      if (u->name() == name) {
        removed = true;
        return true;
      }
      return false;
    }
  );

  return removed;
}

} // namespace battle_city_cpp